package edu.ucsd.cs110s.temperature;
public abstract class Temperature
{
	private float value; 

	public Temperature(float v)
	{

		value = v; 

	}
	
	public final float getValue()
	{
		return value;
	}
	
	public final void setValue(float v)
	{
		value = v;
	}
	
	public abstract Temperature toCelsius();
	
	public abstract Temperature toFahrenheit();
}
