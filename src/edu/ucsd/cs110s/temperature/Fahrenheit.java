package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t); 
	}
	
	public String toString()
	{
		String temperature = Float.toString(getValue());
		return temperature; 
	}

	@Override
	public Temperature toCelsius() {
		float tempC = (getValue()-32)*5/9;
		setValue(tempC);
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		
		return this;
	}
}
