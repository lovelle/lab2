package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature  
{
	public Celsius(float t)
	{
		super(t);
	}

	public String toString()
	{
		String temperature = Float.toString(getValue());
		return temperature;
	}

	@Override
	public Temperature toCelsius() {

		return this; 

	}

	@Override
	public Temperature toFahrenheit() {

		float tempF = 9*getValue()/5+32;
		setValue(tempF);

		return this;
	}
}
